﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TempoMaticClientLib;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            MessageManager mm = new MessageManager();
            mm.RecvTempo = "1001";
            Assert.AreEqual(mm.RecvTempo, "1002");

        }

        [TestMethod]
        public void ListMessageTest01()
        {
            MessageManager mm = new MessageManager();
            mm.RecvTempo = "1001";
            mm.ListMessage(DateTime.Parse("2020-01-01"));

        }
            
    }
}
