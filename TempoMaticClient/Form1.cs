﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TempoMaticClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.UseWaitCursor = true;

            try
            {
                //送信するファイルのパス
                string filePath = this.fileNameText.Text;
                string fileName = System.IO.Path.GetFileName(filePath);

                var message =
                  TempoMaticClientLib.MessageManager.Instance;
                message.Title = this.titleText.Text;
                message.Content = this.contentText.Text;
                message.RecvTempo = this.tenpoText.Text;

                message.SendMessageWithFile(this.fileNameText.Text);

            }
            catch (Exception exp)
            {
                MessageBox.Show(this, "エラー発生", exp.GetType().Name + Environment.NewLine +
                    exp.StackTrace);
            }
            finally
            {
                this.UseWaitCursor = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var res = this.openFileDialog1.ShowDialog();
            if (res != System.Windows.Forms.DialogResult.Cancel)
            {
                this.fileNameText.Text = this.openFileDialog1.FileName;
            }
        }
    }
}
