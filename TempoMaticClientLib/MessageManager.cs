﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempoMaticClientLib
{
    public enum MessageCategoryGroup
    {
        MessageKind = 1070,

    }

    //public static class MessageCategoryGroup
    //{
    //    public static string FilterKind { get { return "1070"; } }

    //}
    public class MessageManager
    {
        private static MessageManager instance;
        public static MessageManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MessageManager();
                }

                return instance;
            }
        }

        public Dictionary<MessageCategoryGroup, string> Categories = new Dictionary<MessageCategoryGroup,string>();

        public void AddCategory(MessageCategoryGroup group, string value)
        {
            if (this.Categories.ContainsKey(group))
            {
                this.Categories[group] = value;
            }
            else
            {
                this.Categories.Add(group, value);
            }
        }

        public Dictionary<string, object> Parameters = new Dictionary<string, object>();

        public void AddParameter(string key, object value)
        {
            if (this.Parameters.ContainsKey(key))
            {
                this.Parameters[key] = value;
            }
            else
            {
                this.Parameters.Add(key, value );
            }
        }

        public string Title { get; set; }
        public string Content { get; set; }
        private string recvTempo;

        public string RecvTempo
        {
            get { return recvTempo; }
            set
            {
                switch (value.Substring(value.Length - 4, 4))
                {
                    case "1001":
                    case "1002":
                    case "1005":
                        recvTempo = "1002";
                        break;
                    default:
                        recvTempo = ("0000" + value).Substring(("0000" + value).Length - 4, 4);
                        break;
                }
            }
        }
        


        public void SendMessageWithFile (string filePath)
        {
            if (this.Title == null) throw new ArgumentNullException("件名が指定されていません。");
            if (string.IsNullOrEmpty(this.RecvTempo)) throw new ApplicationException("送り先店舗が指定されていません。");

            //送信するファイルのパス
            string fileName = System.IO.Path.GetFileName(filePath);

            //送信先のURL
            string url = "https://fukuda-web.tempomatic.jp/h2/STRNoticeManipurator.do";
            //文字コード
            System.Text.Encoding enc =
                System.Text.Encoding.GetEncoding("UTF-8");
            //区切り文字列
            string boundary = System.Environment.TickCount.ToString();

            //WebRequestの作成
            System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)
                System.Net.WebRequest.Create(url);
            //メソッドにPOSTを指定
            req.Method = "POST";
            //ContentTypeを設定
            req.ContentType = "multipart/form-data; boundary=" + boundary;

            DateTime sevenDaysLater = DateTime.Today.AddDays(7);
            //POST送信するデータを作成
            string postData = "";
            postData = 
                "--" + boundary + "\r\n" +
                "Content-Disposition: form-data; name=\"key\"\r\n\r\n" +
               "2QHF-8471-XJFM-058X-DU6Y-1K9S-AFUW-WE9E\r\n"+
             //送信するデータ（フィールド名と値の組み合わせ）を追加
                this.addParts(boundary, "func", "create") +
                this.addParts(boundary, "title", this.Title) +
                this.addParts(boundary, "content", this.Content) +
                this.addParts(boundary,"recvStore", this.RecvTempo) +
                this.addParts(boundary,"openDate", DateTime.Today.ToString("yyyy-MM-dd")) +
                this.addParts(boundary, "closeDate", sevenDaysLater.ToString("yyyy-MM-dd")) +
                this.addParts(boundary,"beginDate", DateTime.Today.ToString("yyyy-MM-dd")) +
                this.addParts(boundary,"endDate", sevenDaysLater.ToString("yyyy-MM-dd")) ;
            foreach(KeyValuePair<MessageCategoryGroup, string> cat in this.Categories){
                int categoryId = (int)cat.Key;
                postData += this.addParts(boundary,string.Format("category({0})", categoryId), cat.Value);
            }
            foreach (KeyValuePair<string, object> parameter in this.Parameters)
            {
                string strValue = parameter.Value is DateTime ?
                    ((DateTime)parameter.Value).ToString("yyyy-MM-dd") : parameter.Value.ToString();
                postData += this.addParts(boundary, key: parameter.Key, value: strValue);
            }

            postData +=
                //this.addParts(boundary,"pdf", fileName) +
                "--" + boundary + "\r\n" +
                "Content-Disposition: form-data; name=\"pdf\"; filename=\"" + fileName + "\"\r\n" +
                "Content-Type: application/octet-stream\r\n" +
                "Content-Transfer-Encoding: binary\r\n\r\n";
            //バイト型配列に変換
            byte[] startData = enc.GetBytes(postData);
            postData = "\r\n--" + boundary + "--\r\n";
            byte[] endData = enc.GetBytes(postData);

            //送信するファイルを開く
            System.IO.FileStream fs = new System.IO.FileStream(
                filePath, System.IO.FileMode.Open,
                System.IO.FileAccess.Read);

            //POST送信するデータの長さを指定
            req.ContentLength = startData.Length + endData.Length + fs.Length;

            //データをPOST送信するためのStreamを取得/
            System.IO.Stream reqStream = req.GetRequestStream();
            //送信するデータを書き込む
            reqStream.Write(startData, 0, startData.Length);
            //ファイルの内容を送信
            byte[] readData = new byte[0x1000];
            int readSize = 0;
            while (true)
            {
                readSize = fs.Read(readData, 0, readData.Length);
                if (readSize == 0)
                    break;
                reqStream.Write(readData, 0, readSize);
            }
            fs.Close();
            reqStream.Write(endData, 0, endData.Length);
            reqStream.Close();

            //サーバーからの応答を受信するためのWebResponseを取得
            System.Net.HttpWebResponse res =
                (System.Net.HttpWebResponse) req.GetResponse();

            //応答データを受信するためのStreamを取得
            System.IO.Stream resStream = res.GetResponseStream();

            //受信して表示
            System.IO.StreamReader sr =
                new System.IO.StreamReader(resStream, enc);
            string result = sr.ReadToEnd();
            Console.WriteLine(result);

            //閉じる
            sr.Close();
 
        }

        public void DeleteMessage(string id)
        {
            //送信先のURL
            string url = "https://fukuda-web.tempomatic.jp/h2/STRNoticeManipurator.do";
            //文字コード
            System.Text.Encoding enc =
                System.Text.Encoding.GetEncoding("UTF-8");
            //区切り文字列
            string boundary = System.Environment.TickCount.ToString();

            //WebRequestの作成
            System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)
                System.Net.WebRequest.Create(url);
            //メソッドにPOSTを指定
            req.Method = "POST";
            //ContentTypeを設定
            req.ContentType = "multipart/form-data; boundary=" + boundary;

            DateTime sevenDaysLater = DateTime.Today.AddDays(7);
            //POST送信するデータを作成
            string postData = "";
            postData = 
                "--" + boundary + "\r\n" +
                "Content-Disposition: form-data; name=\"key\"\r\n\r\n" +
               "2QHF-8471-XJFM-058X-DU6Y-1K9S-AFUW-WE9E\r\n"+
             //送信するデータ（フィールド名と値の組み合わせ）を追加
                this.addParts(boundary, "func", "delete") +
                this.addParts(boundary,"id", id) ;

            //バイト型配列に変換
            byte[] startData = enc.GetBytes(postData);
            //postData = "\r\n--" + boundary + "--\r\n";
            byte[] endData = enc.GetBytes(postData);

            //POST送信するデータの長さを指定
            req.ContentLength = startData.Length + endData.Length;

            //データをPOST送信するためのStreamを取得/
            System.IO.Stream reqStream = req.GetRequestStream();
            //送信するデータを書き込む
            reqStream.Write(startData, 0, startData.Length);
            reqStream.Write(endData, 0, endData.Length);
            reqStream.Close();

            //サーバーからの応答を受信するためのWebResponseを取得
            System.Net.HttpWebResponse res =
                (System.Net.HttpWebResponse) req.GetResponse();

            //応答データを受信するためのStreamを取得
            System.IO.Stream resStream = res.GetResponseStream();

            //受信して表示
            System.IO.StreamReader sr =
                new System.IO.StreamReader(resStream, enc);
            string result = sr.ReadToEnd();
            Console.WriteLine(result);

            //閉じる
            sr.Close();
 
        }


        public void ListMessage(DateTime date)
        {
            //送信先のURL
            string url = "https://fukuda-web.tempomatic.jp/h2/STRNoticeFinder.do";
            //文字コード
            System.Text.Encoding enc =
                System.Text.Encoding.GetEncoding("UTF-8");
            //区切り文字列
            string boundary = System.Environment.TickCount.ToString();

            //WebRequestの作成
            System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)
                System.Net.WebRequest.Create(url);
            //メソッドにPOSTを指定
            req.Method = "POST";
            //ContentTypeを設定
            req.ContentType = "multipart/form-data; boundary=" + boundary;

            DateTime sevenDaysLater = DateTime.Today.AddDays(7);
            //POST送信するデータを作成
            string postData = "";
            postData = 
                "--" + boundary + "\r\n" +
                "Content-Disposition: form-data; name=\"key\"\r\n\r\n" +
               "2QHF-8471-XJFM-058X-DU6Y-1K9S-AFUW-WE9E\r\n"+
             //送信するデータ（フィールド名と値の組み合わせ）を追加
                this.addParts(boundary, "func", "findByPeriod") +
                this.addParts(boundary, "storeCode", this.RecvTempo) +
                this.addParts(boundary, "date", date.ToString("yyyy-MM-dd"));

            //バイト型配列に変換
            byte[] startData = enc.GetBytes(postData);
            //postData = "\r\n--" + boundary + "--\r\n";
            byte[] endData = enc.GetBytes(postData);

            //POST送信するデータの長さを指定
            req.ContentLength = startData.Length + endData.Length;

            //データをPOST送信するためのStreamを取得/
            System.IO.Stream reqStream = req.GetRequestStream();
            //送信するデータを書き込む
            reqStream.Write(startData, 0, startData.Length);
            reqStream.Write(endData, 0, endData.Length);
            reqStream.Close();

            //サーバーからの応答を受信するためのWebResponseを取得
            System.Net.HttpWebResponse res =
                (System.Net.HttpWebResponse) req.GetResponse();

            //応答データを受信するためのStreamを取得
            System.IO.Stream resStream = res.GetResponseStream();

            //受信して表示
            System.IO.StreamReader sr =
                new System.IO.StreamReader(resStream, enc);
            string result = sr.ReadToEnd();
            Console.WriteLine(result);

            //閉じる
            sr.Close();
 
        }





        private string addParts(string boundary, string key, string value)
        {
            return  string.Format(
            "--{0}\r\n" +
            "Content-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}\r\n"
                , boundary, key, value);

        }


    }
}
